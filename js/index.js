import 'babel-polyfill';


//React
import React from 'react'
import {render} from 'react-dom';

//containers
import App from './containers/App';



//Begin Rendering the page
render(
  <App />,
  document.getElementById('content')
)
