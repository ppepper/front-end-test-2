import React from 'react';
import PropTypes from 'prop-types';

import {Jumbotron, Button, Grid, Row, Col, Tabs, Tab} from 'react-bootstrap';

import VideoTabContent from '../containers/VideoTabContent';



export default function VideosPage({isContentLoaded, isContentLoading, isContentLoadError, content, contentLoadError}) {

  return isContentLoaded ? <div>
    <Jumbotron>
      <Grid>
        <Row>
          <Col>
            <h1>{content.mcd.title}</h1>
          </Col>
        </Row>
      </Grid>
    </Jumbotron>
    <Grid>
      <Row>
        <Col>
          <Tabs defaultActiveKey={0} id="videos_tabs">
            {content.mcd.tab_creator.split(',').map((tabId, index) => {
              const tab = content.tabs[tabId];

              return <Tab eventKey={index} key={tabId} title={tab.tab_name}>{/*this doesn't work: tabClassName={tab.classes}*/}
                <VideoTabContent id={tabId} content={content.content[tabId]} />
              </Tab>
            })}
          </Tabs>
        </Col>
      </Row>
    </Grid>
  </div> : null
}

VideosPage.propTypes = {
  isContentLoaded: PropTypes.bool.isRequired,
  isContentLoading: PropTypes.bool.isRequired,
  isContentLoadError: PropTypes.bool.isRequired,
  content: PropTypes.object, //TODO properly define content structure
  contentLoadError: PropTypes.instanceOf(Error)
};
