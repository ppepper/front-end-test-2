import React from 'react';

import {Grid, Row, Col, Tabs, Tab, Image, Pagination} from 'react-bootstrap';


function chunk(array, chunkSize) {
  const chunks = [];

  for (let i = 0, j = array.length; i < j; i += chunkSize) {
      chunks.push(array.slice(i, i + chunkSize));
  }

  return chunks;
}

function numberRangeExclusive(start, end, func) {
  for (let i = start; i < end; i++) {
    func(i);
  }
}

export default function VideoTabContent({id, content, perPage = 12, perRow = 4, currentPageNum = 0, doSetPage = null}) {
  //paginationItems
  const paginationItems = [];
  const numPages = Math.ceil(content.length / perPage);

  numberRangeExclusive(0, numPages, (i) => {
    paginationItems.push(
      <Pagination.Item key={i} active={i === currentPageNum} onClick={(e) => {
        e.preventDefault();

        doSetPage && doSetPage(i);
      }}>{(i+1)}</Pagination.Item>
    );
  });

  const pageContents = content.slice(currentPageNum * perPage, (currentPageNum + 1) * perPage);

  return <Grid>
    {chunk(pageContents, perRow).map((row, index) => {
      return <Row key={index}>
        {row.map((item, index) => {
          return <Col md={Math.floor(12 / perRow)} key={index}>
            <article className="video-thumbnail">
              <Image src={item.image_url} thumbnail />
              <h3 className="video-thumbnail-title">{item.title}</h3>
              <p className="video-thumbnail-date">{item.date}</p>
            </article>
          </Col>
        })}
      </Row>
    })}
    <Row>
      <Col>
        <Pagination bsSize="medium" >{paginationItems}</Pagination>
      </Col>
    </Row>
  </Grid>
}
