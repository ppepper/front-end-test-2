import React from 'react';
import {compose, withStateHandlers} from 'recompose';


//Presentational
import VideoTabContent from '../presentational/VideoTabContent';


export default compose(
  withStateHandlers({
    currentPageNum: 0
  }, {
    doSetPage: () => (newPageNum) => ({
      currentPageNum: newPageNum//TODO validate to ensure remains within valid range
    })
  })
)(
  VideoTabContent
);
