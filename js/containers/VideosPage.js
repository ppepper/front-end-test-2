import React from 'react';
import {compose, withStateHandlers, withPropsOnChange} from 'recompose';
import 'isomorphic-fetch';


//presentational
import VideosPage from '../presentational/VideosPage';

//should be externalised into config file, and use a template e.g. https://titan.asset.tv/api/channel-view-json/[id]
const API_URL = 'https://titan.asset.tv/api/channel-view-json/';


export default compose(
  withStateHandlers(
    {
      content: null,
      currentContentId: null,
      loadingContentId: null,
      isContentLoaded: false,
      isContentLoading: false,
      isContentLoadError: false,
      contentLoadError: null
    }, {
      onContentLoadStart: () => (id) => ({
        loadingContentId: id,
        isContentLoading: true
      }),
      onContentLoadComplete: ({loadingContentId}) => (content) => ({
        isContentLoaded: true,
        isContentLoading: false,
        currentContentId: loadingContentId,
        loadingContentId: null,
        isContentLoadError: false,
        contentLoadErrorMsg: null,
        content
      }),
      onContentLoadError: () => (error) => ({
        isContentLoading: false,
        isContentLoadError: true,
        contentLoadErrorMsg: error
      })
    }),
  withPropsOnChange(
    ['match'],
    ({match, onContentLoadStart, onContentLoadComplete, onContentLoadError}) => {
      const id = match.params.id;

      onContentLoadStart(id);//record that content is being loaded

      fetch(`${API_URL}${id}`)
        .then(response => {
          //Check if load was OK
          if (response.status >= 400) {
            throw new Error("Bad response from server");
          }

          //attempt to parse returned JSON
          return response.json();
        })
        .then(onContentLoadComplete)
        .catch(onContentLoadError)
    }
  )
)(VideosPage);
