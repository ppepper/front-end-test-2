import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import {Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

//containers
import VideosPage from './VideosPage';

//Presentational
import RenderChildren from '../presentational/RenderChildren';


const DEFAULT_VIDEOS_ID = '2240';


//Handles routing of URL parameters
export default function App() {
  return <Router basename="/html">
    <RenderChildren>{/*Required to deal with Router limitation that it only allows a single child (a pre react v16 limititaion), without adding suprious markup*/}
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/html">Example</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem eventKey={1} href="#">
              Link
            </NavItem>
            <NavItem eventKey={2} href="#">
              Link
            </NavItem>
            <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1}>Action</MenuItem>
              <MenuItem eventKey={3.2}>Another action</MenuItem>
              <MenuItem eventKey={3.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.4}>Separated link</MenuItem>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Route path="/:id" component={VideosPage}/>
      <Route exact path="/" render={() => (//If no ID specified in URL, use default ID (could be externalised into config file)
        <VideosPage match={{params: {id: DEFAULT_VIDEOS_ID}}} />
      )}/>
    </RenderChildren>
  </Router>
}
