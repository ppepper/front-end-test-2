const gulp = require('gulp');
const gutil = require('gulp-util')
const runSequence = require('run-sequence');
const webpackStream = require('webpack-stream');
const webpack = require('webpack');
require('script-loader');
require('json-loader');
const sass = require('gulp-sass');
const filter = require('gulp-filter');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const del = require('del');
const notify = require("gulp-notify");
const plumber = require("gulp-plumber");
const fs = require('fs-extra');
const stripDebug = require('gulp-strip-debug');
const gulpWatch = require('gulp-watch');

const watch = gulpWatch;//gulp.watch;

const config = {
  devOutputPath:'develop',
  buildOutputPath:'build',
  autoprefixer: {
    browsers: require('./browsers')
  },
  sass: {
    includePaths: ['node_modules']
  },
  cssnano: {
    dev: {},
    build: {}
  }
};

const npmConfig = require('./package.json');


config.css = {
  devOutputPath: config.devOutputPath + '/css',
  buildOutputPath: config.buildOutputPath + '/css',
  watch: ['scss/**'],
  src: 'scss/**/*.+(css|scss)',
  devSourcemaps: {
    enabled: true
  },
  buildSourcemaps: {
    enabled: false
  }
};

config.html = {
  devOutputPath: config.devOutputPath + '/html',
  buildOutputPath: config.buildOutputPath + '/html',
  watch: ['html/**/*.+(json|html)'],
  src: 'html/**/*.+(json|html)',
  devSourcemaps: {
    enabled: true
  },
  buildSourcemaps: {
    enabled: false
  }
};


config.js = {
  devOutputPath: config.devOutputPath + '/js',
  buildOutputPath: config.buildOutputPath + '/js',
  watch: ['js/**/*.+(js|json)'],
  entry: npmConfig.main,//used by webpack, instead of src
  devSourcemaps: {
    enabled: true
  },
  buildSourcemaps: {
    enabled: false
  }
};

config.notify = {
  js: 'Error: <%= error.message %>',
  css: 'Error: <%= error.message %>'
};

config.plumber = {
  js: {
    errorHandler: notify.onError(config.notify.js)
  },
  css: {
    errorHandler: notify.onError(config.notify.css)
  }
};

config.uglify = {
  build: {}
};

config.browserSync = {
    server: {
        baseDir: "./"+config.devOutputPath
    },
    middleware: function(req, res, next) {
      //rewrite URLs for dev purposes
      if(req.url.indexOf('/html/') === 0) {
        req.url = '/html/index.html';
      }

      return next();
    },
    startPath: 'html/',
    ghostMode:false,
    notify:false,
    reloadDelay: 200,
    reloadDebounce: 500
};

config.webpack = {
  dev: {
    devtool: 'source-map',
    output: {
        path: __dirname + "/"+config.js.devOutputPath,
        filename: "index.js"
    },
    module: {
      loaders: [
        {
          test: /.*.json$/,
          loader: 'json'
        },
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: [
              ['env', {
              'targets': {
                browsers: require('./browsers')
                }
              }], 'react'
            ],
            plugins: ['transform-runtime', 'transform-object-rest-spread', 'transform-class-properties']
          }
        }
      ]
    }
  },
  build: {
    devtool: (config.js.buildSourcemaps.enabled ? 'source-map' : null),
    output: {
        path: __dirname + "/"+config.js.buildOutputPath,
        filename: "index.js"
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      })
    ],
    module: {
      loaders: [
        {
          test: /.*.json$/,
          loader: 'json'
        },
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: [
              ['env', {
              'targets': {
                browsers: require('./browsers')
                }
              }], 'react'
            ],
            plugins: ['transform-runtime', 'transform-object-rest-spread', 'transform-class-properties']
          }
        }
      ]
    }
  }
};


config.copy = {
  dev: {
    //fonts: config.devOutputPath+'/fonts'
  },
  build: {
    //fonts: config.buildOutputPath+'/fonts'
  }
}

config.copy.watch = (() => {
  const allKeys = {};

  const mergeFunc = (allKeys, key) => {
    allKeys[key] = key;
    return allKeys;
  };

  Object.keys(config.copy.dev).reduce(mergeFunc, allKeys);
  Object.keys(config.copy.build).reduce(mergeFunc, allKeys);

  return Object.keys(allKeys).map((src) => {
    return src+'/**/*';
  });
})();

var isBrowserSync = false;

//General functions
function gulpJsSrc() {
  return gulp.src(config.js.entry);
}

function gulpCssSrc() {
  return gulp.src(config.css.src);
}

function gulpHtmlSrc() {
  return gulp.src(config.html.src);
}

//Define tasks

//-JS tasks
gulp.task('js-dev', ['js-clean-dev'], () => {
  return gulpJsSrc()
    .pipe(plumber(config.plumber.js))
    .pipe(config.js.devSourcemaps.enabled ? sourcemaps.init(config.js.devSourcemaps.options) : filter(['**/*']))
    .pipe(webpackStream(config.webpack.dev))
    .pipe(config.js.devSourcemaps.enabled ? sourcemaps.write(config.js.devSourcemaps.path, config.js.devSourcemaps.writeOptions) : filter(['**/*']))
    .pipe(gulp.dest(config.js.devOutputPath+'/'));
});

gulp.task('js-watch', ['js-dev'], () => {
  return watch(config.js.watch, () => {
    runSequence('js-dev', 'dev-server-refresh');
  });
});

gulp.task('js-build', ['js-clean-build'], function() {
  return gulpJsSrc()
    .pipe(plumber(config.plumber.js))
    .pipe(webpackStream(config.webpack.build))
    .pipe(stripDebug())
    .pipe(uglify())
      .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(gulp.dest(config.js.buildOutputPath+'/'));
});

gulp.task('js-build-no-mini', ['js-clean-build'], function() {
  return gulp.src(config.js.entry)
    .pipe(webpackStream(config.webpack.build))
    .pipe(gulp.dest(config.js.buildOutputPath+'/'));
});

gulp.task('js-clean-dev', (done) => {
  del([config.js.devOutputPath+'/*.*']).then(() => {done()});
});

gulp.task('js-clean-build', (done) => {
  del([config.js.buildOutputPath+'/*.*'], {force: true}).then(() => {done()});
});

//-CSS tasks
function cssDev() {
  return gulpCssSrc()
    .pipe(plumber(config.plumber.css))
    .pipe(config.css.devSourcemaps.enabled ? sourcemaps.init(config.css.devSourcemaps.options) : filter(['**/*']))
    .pipe(sass(config.sass))
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(config.css.devSourcemaps.enabled ? sourcemaps.write(config.css.devSourcemaps.path, config.css.devSourcemaps.writeOptions) : filter(['**/*']))
    .pipe(gulp.dest(config.css.devOutputPath+'/'));
}

gulp.task('css-dev', ['css-clean-dev'], function() {
  return cssDev();
});

gulp.task('css-watch', ['css-dev'], () => {
  return watch(config.css.watch, () => {
    return isBrowserSync ? cssDev().pipe(browserSync.stream()) : cssDev();
  });
});

gulp.task('css-build', ['css-clean-build'], function() {
  return gulpCssSrc()
    .pipe(plumber(config.plumber.css))
    .pipe(config.css.buildSourcemaps.enabled ? sourcemaps.init(config.css.buildSourcemaps.options) : filter(['**/*']))
    .pipe(sass(config.sass))
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(cssnano(config.cssnano.build))
    .pipe(config.css.buildSourcemaps.enabled ? sourcemaps.write(config.css.buildSourcemaps.path, config.css.devSourcemaps.writeOptions) : filter(['**/*']))
    .pipe(gulp.dest(config.css.buildOutputPath+'/'));
});

gulp.task('css-clean-dev', (done) => {
  del([config.css.devOutputPath+'/*.*']).then(() => {done()});
});

gulp.task('css-clean-build', (done) => {
  del([config.css.buildOutputPath+'/*.*'], {force: true}).then(() => {done()});
});


//-HTML tasks
gulp.task('html-dev', ['html-clean-dev'], function() {
  return gulpHtmlSrc().pipe(gulp.dest(config.html.devOutputPath+'/'));
});

gulp.task('html-watch', ['html-dev'], () => {
  return watch(config.html.watch, () => {
    runSequence('html-dev', 'dev-server-refresh');
  });
});

gulp.task('html-build', ['html-clean-build'], () => {
  return gulpHtmlSrc().pipe(gulp.dest(config.html.buildOutputPath+'/'));
});

gulp.task('html-clean-dev', (done) => {
  del([config.html.devOutputPath]).then(() => {done()});
});

gulp.task('html-clean-build', (done) => {
  del([config.html.buildOutputPath]).then(() => {done()});
});


//-copy tasks
gulp.task('copy-dev', ['copy-clean-dev'], (done) => {
  let promises = Object.keys(config.copy.dev).map((src) => {
    let dest = config.copy.dev[src];

    return fs.copy(src, dest);
  });

  Promise.all(promises).then((values) => {
    done();
  });
});

gulp.task('copy-build', ['copy-clean-build'], (done) => {
  let promises = Object.keys(config.copy.build).map((src) => {
    let dest = config.copy.build[src];

    return fs.copy(src, dest);
  });

  Promise.all(promises).then((values) => {
    done();
  });
});

gulp.task('copy-watch', ['copy-dev'], () => {
  return watch(config.copy.watch, () => {
    runSequence('copy-dev', 'dev-server-refresh');
  });
});

gulp.task('copy-clean-dev', (done) => {
  const delDirs = Object.values(config.copy.dev).map((dir) => {
    return dir+'/**/*';
  });

  del(delDirs).then(() => {done()});
});



gulp.task('copy-clean-build', (done) => {
  const delDirs = Object.values(config.copy.build).map((dir) => {
    return dir+'/*.*';
  });

  del(delDirs).then(() => {done()});
});

//General tasks
const devTasks =    ['js-dev', 'css-dev', 'html-dev', 'copy-dev'];
const buildTasks =  ['js-build', 'css-build', 'copy-build'];
const watchTasks =  [['js-watch', 'css-watch', 'html-watch', 'copy-watch']];

//A one-off dev build
gulp.task('dev', (done) => {
  runSequence.apply(null, devTasks.concat(done));
});

//run a dev build, then watch for future changes & continue dev building
gulp.task('watch', () => {
  runSequence.apply(null, watchTasks);
});

//like watch, but also runs a development web server with browserSync
gulp.task('dev-server', ['dev'], () => {
  browserSync.init(config.browserSync);
  isBrowserSync = true;
  runSequence('watch');
});

//Needed for internal use only
gulp.task('dev-server-refresh', (done) => {
  if(isBrowserSync) {
    browserSync.reload();
  }
  done();
});

//A one-off full build (includes minification, etc)
gulp.task('build', (done) => {
  runSequence.apply(null, buildTasks.concat(done));
});
